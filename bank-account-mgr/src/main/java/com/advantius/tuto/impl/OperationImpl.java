package com.advantius.tuto.impl;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.advantius.tuto.dao.CompteRepository;
import com.advantius.tuto.dao.OperationRepository;
import com.advantius.tuto.entities.Comptes;
import com.advantius.tuto.entities.Operations;
import com.advantius.tuto.entities.Retraits;
import com.advantius.tuto.entities.Versements;
import com.advantius.tuto.service.OperationService;
import com.advantius.tuto.service.PageOperations;

@Service
public class OperationImpl implements OperationService{

	@Autowired
	OperationRepository operationDao;
	
	@Autowired
	CompteRepository compteDao;
	
	@Override
	public PageOperations getOperations(String numCompte, int page, int size) {
		Page<Operations> listPagesOpe = operationDao.findOpByCompte(numCompte, PageRequest.of(page, size, new Sort(Direction.DESC, "dateOp")));
		PageOperations pageOpe = new PageOperations();
		pageOpe.setOperations(listPagesOpe.getContent());
		pageOpe.setNombreOpe(listPagesOpe.getNumberOfElements());
		pageOpe.setPage(listPagesOpe.getNumber());
		pageOpe.setTotalOpe(listPagesOpe.getTotalElements());
		pageOpe.setTotalPages(listPagesOpe.getTotalPages());
		return pageOpe;
	}

	@Override
	public void retirer(String numCompte, double montant) {
		Comptes compte = compteDao.getCompte(numCompte);
		compte.setSolde(compte.getSolde() - montant);
		operationDao.save(new Retraits(compte, LocalDate.now(), montant));
	}

	@Override
	public void verser(String numCompte, double montant) {
		Comptes compte = compteDao.getCompte(numCompte);
		compte.setSolde(compte.getSolde() + montant);
		operationDao.save(new Versements(compte, LocalDate.now(), montant));
	}

	@Override
	public void virement(String cpte1, String cpte2, double montant) {
		retirer(cpte1, montant);
		verser(cpte2, montant);
	}

}
