package com.advantius.tuto.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.advantius.tuto.dao.ClientRepository;
import com.advantius.tuto.entities.Clients;
import com.advantius.tuto.service.ClientService;

@Service
public class ClientImpl implements ClientService{
	
	@Autowired
	private ClientRepository clientDao;

	@Override
	public Clients addClient(Clients client) {
		return clientDao.save(client);
	}

	@Override
	public List<Clients> listClients() {
		return clientDao.findAll();
	}

	@Override
	public List<Clients> findClients(String nom) {
		return clientDao.findClients(nom);
	}
}
