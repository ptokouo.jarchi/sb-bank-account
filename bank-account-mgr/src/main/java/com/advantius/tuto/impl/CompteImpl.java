package com.advantius.tuto.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.advantius.tuto.dao.CompteRepository;
import com.advantius.tuto.entities.Comptes;
import com.advantius.tuto.service.CompteService;

@Service
public class CompteImpl implements CompteService{

	@Autowired
	CompteRepository compteDao;
	
	@Override
	public Comptes addCompte(Comptes compte) {
		return compteDao.save(compte);
	}

	@Override
	public Comptes getCompte(String numCompte) {
		return compteDao.getCompte(numCompte);
	}	
}
