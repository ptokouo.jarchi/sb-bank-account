package com.advantius.tuto.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.advantius.tuto.entities.Comptes;

public interface CompteRepository extends JpaRepository<Comptes, String>{
	
	@Query("select o from Comptes o where o.numCompte = :x")
	public Comptes getCompte(@Param("x") String numCompte);
}
