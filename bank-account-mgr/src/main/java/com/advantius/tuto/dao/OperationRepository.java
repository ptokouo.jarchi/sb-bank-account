package com.advantius.tuto.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.advantius.tuto.entities.Operations;

public interface OperationRepository extends JpaRepository<Operations, Long>{

	@Query("select o from Operations o where o.compte.numCompte = :x")
	public Page<Operations> findOpByCompte(@Param("x") String compte, Pageable pageable);
}