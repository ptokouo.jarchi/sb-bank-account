package com.advantius.tuto.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.advantius.tuto.entities.Clients;

public interface ClientRepository extends JpaRepository<Clients, Long>{
	
	@Query("select o from Clients o where o.nom = :x")
	public List<Clients> findClients(@Param("x") String nom);
}
