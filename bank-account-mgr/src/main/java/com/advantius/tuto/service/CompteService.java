package com.advantius.tuto.service;

import com.advantius.tuto.entities.Comptes;

public interface CompteService {
	public Comptes addCompte(Comptes compte);	
	public Comptes getCompte(String numCompte);
}
