package com.advantius.tuto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.advantius.tuto.entities.Comptes;

@RestController
public class CompteRestService {
	
	@Autowired
	private CompteService compteServ;

	// http://localhost:8090/Banque/compte/111506376
	@RequestMapping(value = "/Banque/compte/{codeCompte}", method = RequestMethod.GET)
	public Comptes getCompte(@PathVariable("codeCompte") String numCompte) {
		System.out.println(numCompte);
		return compteServ.getCompte(numCompte);
	}
}
