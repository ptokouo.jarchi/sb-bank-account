package com.advantius.tuto.service;

public interface OperationService {

	public void retirer(String numCompte, double montant);
	
	public void verser(String numCompte, double montant);
	
	public void virement(String cpte1, String cpte2, double montant);
	
	public PageOperations getOperations(String numCompte, int page, int size);
	
	
}
