package com.advantius.tuto.service;

import java.util.List;

import com.advantius.tuto.entities.Clients;

public interface ClientService {
	public Clients addClient(Clients client);
	public List<Clients> listClients();
	public List<Clients> findClients(String nom);
}
