package com.advantius.tuto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OperationRestService {

	@Autowired
	private OperationService opeServ;

	@RequestMapping(value = "/Banque/operations/retraits", method = RequestMethod.PUT)
	public void retirer(@RequestParam("compte") String numCompte, 
			@RequestParam("montant") double montant) {
		opeServ.retirer(numCompte, montant);
	}

	@RequestMapping(value = "/Banque/operations/versements", method=RequestMethod.PUT)
	public void verser(@RequestParam("compte") String numCompte, 
			@RequestParam("montant") double montant) {
		opeServ.verser(numCompte, montant);
	}

	@RequestMapping(value = "/Banque/operations/virements", method=RequestMethod.PUT)
	public void virement(@RequestParam("compte") String cpte1, 
			@RequestParam("compte2") String cpte2, 
			@RequestParam("montant") double montant) {
		opeServ.virement(cpte1, cpte2, montant);
	}

	// http://localhost:8090/Banque/operations?compte=111506376&page=2&size=5
	@RequestMapping(value = "/Banque/operations", method = RequestMethod.GET)
	public PageOperations getOperations(@RequestParam("compte") String numCompte, 
			@RequestParam("page") int page, @RequestParam("size") int size) {
		return opeServ.getOperations(numCompte, page, size);
	}		
}