package com.advantius.tuto.service;

import java.io.Serializable;
import java.util.List;

import com.advantius.tuto.entities.Operations;

public class PageOperations implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Operations> operations;
	
	private int nombreOpe;
	
	private long totalOpe;
	
	private int totalPages;
	
	private int page;

	public List<Operations> getOperations() {
		return operations;
	}

	public void setOperations(List<Operations> operations) {
		this.operations = operations;
	}

	public int getNombreOpe() {
		return nombreOpe;
	}

	public void setNombreOpe(int nombreOpe) {
		this.nombreOpe = nombreOpe;
	}

	public long getTotalOpe() {
		return totalOpe;
	}

	public void setTotalOpe(long totalOpe) {
		this.totalOpe = totalOpe;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}
}
