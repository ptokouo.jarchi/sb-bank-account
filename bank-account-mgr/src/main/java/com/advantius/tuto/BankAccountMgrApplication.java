package com.advantius.tuto;

import java.time.LocalDate;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.advantius.tuto.entities.Clients;
import com.advantius.tuto.entities.Comptes;
import com.advantius.tuto.entities.ComptesCourant;
import com.advantius.tuto.entities.ComptesEpargne;
import com.advantius.tuto.service.ClientService;
import com.advantius.tuto.service.CompteService;
import com.advantius.tuto.service.OperationService;
import com.advantius.tuto.service.PageOperations;

@SpringBootApplication
public class BankAccountMgrApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(BankAccountMgrApplication.class, args);
		ClientService clientServ = context.getBean(ClientService.class);
		CompteService compteServ = context.getBean(CompteService.class);
		OperationService opeServ = context.getBean(OperationService.class);

//		System.out.println("Adding clients --------------------");
//		clientServ.addClient(new Clients("Patrick R."));
//		clientServ.addClient(new Clients("Carine N."));
//		clientServ.addClient(new Clients("Douglas A."));
		
		System.out.println("\n\nListing of all clients --------------------");		
		List<Clients> listClients = clientServ.listClients();
		listClients.forEach(c -> System.out.println(c));
		Clients client1 = listClients.get(0);
		Clients client2 = listClients.get(1);
		
		System.out.println("\nFind clients by name --------");
		List<Clients> list2Clients = clientServ.findClients("Patrick R.");
		list2Clients.forEach(c -> System.out.println(c));
				
//		System.out.println("\n\nAdding Comptes --------------------");
//		compteServ.addCompte(new ComptesCourant("111506376", client1, LocalDate.now(), 100000, 0));
//		compteServ.addCompte(new ComptesCourant("103254125", client1, LocalDate.now(), 55000, 0));
//		compteServ.addCompte(new ComptesEpargne("100510256", client2, LocalDate.now(), 1000000, 5.5));
		
		System.out.println("\nFind compte by num ----------------");
		Comptes compte1 = compteServ.getCompte("111506376");
		Comptes compte2 = compteServ.getCompte("100510256");
		System.out.println(compte1);
		System.out.println(compte2);
		
		
//		System.out.println("\"\"Adding operations ---------------");
//		opeServ.retirer("111506376", 1000000);
//		opeServ.verser("111506376", 5000000);
//		opeServ.virement("111506376", "100510256", 300000);
		
		System.out.println("\n\nFind operations by compte");
		PageOperations pageOpes = opeServ.getOperations("111506376", 2, 6);
		pageOpes.getOperations().forEach(o -> System.out.println(o));		
	}
}