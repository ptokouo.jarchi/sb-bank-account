package com.advantius.tuto.entities;

import java.time.LocalDate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("CC")
public class ComptesCourant extends Comptes{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private double decouvert;
	
	public ComptesCourant() {
		
	}
	
	public ComptesCourant(String num, Clients client, LocalDate dateCrea, double solde, double decouvert){
		super(num, client, dateCrea, solde);
		this.decouvert = decouvert;
	}
	
	public double getDecouvert() {
		return decouvert;
	}
	
	public void setDecouvert(double decouvert) {
		this.decouvert = decouvert;
	}
	
	public String toString(){
		return super.toString() + "\t" + decouvert;
	}
}