package com.advantius.tuto.entities;

import java.time.LocalDate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("VE")
public class Versements extends Operations{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Versements() {

	}

	public Versements(Comptes compte, LocalDate dateOp, double montant){
		super(compte, dateOp, montant);
	}
}
