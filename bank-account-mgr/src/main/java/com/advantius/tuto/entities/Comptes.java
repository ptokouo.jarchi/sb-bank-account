package com.advantius.tuto.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE_CPTE", 
	discriminatorType = DiscriminatorType.STRING, 
	length = 2)
public class Comptes implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "NUM_COMPTE")
	private String numCompte;
	
	@Column(name = "TYPE_CPTE", nullable = false, updatable = false, insertable = false)
	private String typeCpte;
	
	@Column(name = "DATE_CR")
	private LocalDate dateCR;
	
	@Column(name = "SOLDE")
	private double solde;
	
	@ManyToOne
	@JoinColumn(name = "CODE_CLI")
	private Clients client;

	public Comptes() {

	}

	public Comptes(String num, Clients client, LocalDate dateCrea, double solde){
		this.numCompte = num;
		this.client = client;
		this.dateCR = dateCrea;
		this.solde = solde;
	}
	
	public String getNumCompte() {
		return numCompte;
	}

	public void setNumCompte(String numCompte) {
		this.numCompte = numCompte;
	}

	public LocalDate getDateCR() {
		return dateCR;
	}

	public void setDateCR(LocalDate dateCR) {
		this.dateCR = dateCR;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}

	public Clients getClient() {
		return client;
	}

	public void setClient(Clients client) {
		this.client = client;
	}

	public String getTypeCpte() {
		return typeCpte;
	}
	
	public void setTypeCpte(String typeCpte) {
		this.typeCpte = typeCpte;
	}
	
	@Override
	public String toString() {
		return numCompte + "\t" + client.getNom() + "\t" + typeCpte + "\t" + dateCR + "\t" + solde;
	}
	
	
}
