package com.advantius.tuto.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE_OP", 
	discriminatorType = DiscriminatorType.STRING, 
	length = 2)
public class Operations implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "NUM_OP")
	private long numOp;
	
	@Column(name = "TYPE_OP", nullable = false, updatable = false, insertable = false)
	private String typeOp;
	
	@Column(name = "DATE_OP")
	private LocalDate dateOp;
	
	@Column(name = "MONTANT")
	private double montant;
	
	@ManyToOne
	@JoinColumn(name = "NUM_CPTE")
	private Comptes compte;
	
	public Operations() {

	}

	public Operations(Comptes compte, LocalDate dateOp, double montant){
		this.compte = compte;
		this.dateOp = dateOp;
		this.montant = montant;
	}
	
	public long getNumOp() {
		return numOp;
	}

	public void setNumOp(long numOp) {
		this.numOp = numOp;
	}

	public LocalDate getDateOp() {
		return dateOp;
	}

	public void setDateOp(LocalDate dateOp) {
		this.dateOp = dateOp;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public Comptes getCompte() {
		return compte;
	}

	public void setCompte(Comptes compte) {
		this.compte = compte;
	}
	
	public String getTypeOp() {
		return typeOp;
	}
	
	public void setTypeOp(String typeOp) {
		this.typeOp = typeOp;
	}
	
	public String toString() {
		return compte.getNumCompte() + "\t"+ typeOp + "\t" + dateOp + "\t" + montant;
	}
}