package com.advantius.tuto.entities;

import java.time.LocalDate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("CE")
public class ComptesEpargne extends Comptes{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private double taux;

	public ComptesEpargne() {
	
	}
	
	public ComptesEpargne(String num, Clients client, LocalDate dateCrea, double solde, double taux){
		super(num, client, dateCrea, solde);
		this.taux = taux;
	}
	
	public double getTaux() {
		return taux;
	}
	
	public void setTaux(double taux) {
		this.taux = taux;
	}
	
	public String toString(){
		return super.toString() + "\t" + taux;
	}
}
