package com.advantius.tuto.entities;

import java.time.LocalDate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("RE")
public class Retraits extends Operations{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Retraits() {

	}
	
	public Retraits(Comptes compte, LocalDate dateOp, double montant){
		super(compte, dateOp, montant);
	}
}
