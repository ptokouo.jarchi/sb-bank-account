var app = angular.module("bankApp", []);
app.controller("bankController", function($scope, $http){
	$scope.compte = {};
	$scope.operation = {typeOp:"versements", montant:0, compte2:null};
	$scope.pageOperations = [];
	$scope.size = 3;
	$scope.pageCourante = 0;
	$scope.pages = [];
	
	$scope.chargerCompte = function(){ 
		$http({
            method : 'GET',
            url : 'http://localhost:8090/Banque/compte/' + $scope.codeCompte
        }).then(function successCallback(response) {
            $scope.compte = response.data;
            $scope.chargerOperations();
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
	};
	
	$scope.chargerOperations = function(){
		$http({
            method : 'GET',
            url : 'http://localhost:8090/Banque/operations?compte=' + $scope.codeCompte 
            	+ '&page=' + $scope.pageCourante + '&size=' + $scope.size
        }).then(function successCallback(response) {
            $scope.pageOperations = response.data;
            $scope.pages = new Array(response.data.totalPages);
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
	}
	
	$scope.goToPage = function(p){
		$scope.pageCourante = p;
		$scope.chargerOperations();
	}
	
	$scope.saveOperation = function(){ 
		var params = "";
		
		if($scope.operation.typeOp == 'virements'){
			params = '&compte2=' + $scope.operation.compte2;
		}
		
		$http({
            method : 'PUT',
            url : 'http://localhost:8090/Banque/operations/' + $scope.operation.typeOp,
            data: 'compte=' + $scope.codeCompte + '&montant=' + $scope.operation.montant + params,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).then(function successCallback(response) {
            $scope.chargerCompte();
            $scope.chargerOperations();
        }, function errorCallback(response) {
            console.log(response.statusText);
        });
	};
});